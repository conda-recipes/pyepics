pyepics conda recipe
====================

Home: https://github.com/pyepics/pyepics

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: PyEpics 3: Epics Channel Access for Python
